<!DOCTYPE html>
<html>
    <head>
        <META HTTP-EQUIV="REFRESH" CONTENT="5">
        <title>Graficas 1</title>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    </head>
    <body>
        <div style="height: 300px; width: 600px;">
            <canvas id="miGrafica"></canvas>
        </div>
        <script>
            const labels = [
                   'Paso 1',
                   'Paso 2',
                   'Paso 3',
                   'Paso 4',
                   'Paso 5',
                   'Paso 6',
                   'Paso 7',
                   'Paso 8',
                   'Paso 9',
                   'Paso 10'
                    ];

            const data = {
                labels: labels,
                datasets: [{
                      label: 'Gráfico de datos',
                      backgroundColor: 'rgb(255, 99, 132)',
                      borderColor: 'rgb(255, 99, 132)',
                      data: [
        <?php require "./dia.php"  ?>
      ],
    }]
  };

  const config = {
    type: 'line',
    data: data,
    options: {}
  };

  const myChart = new Chart(
    document.getElementById('miGrafica'),
    config
  );

            
        </script>
    </body>
</html>
