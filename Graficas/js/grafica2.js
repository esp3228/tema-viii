const labels = [
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ];

  const data = {
    labels: labels,
    datasets: [{
      label: 'Gráfico de datos',
      backgroundColor: 'rgb(126, 99, 32)',
      borderColor: 'rgb(255, 99, 132)',
      data: [6, 19, 15, 23, 70, 30],
    }]
  };

  const config = {
    type: 'bar',
    data: data,
    options: {}
  };

  const myChart = new Chart(
    document.getElementById('miGrafica'),
    config
  );
