const labels = [
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ];

  const data = {
    labels: labels,
    datasets: [{
      label: 'Gráfico de datos',
      backgroundColor: 'rgb(226, 69, 42)',
      borderColor: 'rgb(255, 99, 132)',
      data: [6, 19, 15, 23, 70, 30],
    }]
  };

  const config = {
    type: 'pie',
    data: data,
    options: { indexAxis: 'y'}
  };

  const myChart = new Chart(
    document.getElementById('miGrafica'),
    config
  );
