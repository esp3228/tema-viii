/*
    Autor: Rafael Herrera García
    Archivo: ejercicio5.js
    Descripción: Uso de modulo bis.
*/

import {lista} from './ejercicio5m.js';

let accion=function (opc){
  let cadena;
  let e=document.getElementById("numero");
  if(opc==1){
    lista.addItem(parseFloat(e.value));
    e.value=1;
    cadena=`Item agregado`;
  }else if(opc==7)
    cadena=`La cantidad de elementos=${lista.count()}`;
  else{
    if(!isFree())   
       switch(opc){
          case 2:cadena=`${e.value} ${lista.searchItem(e.value)?"si":"no"} se localiza`;
                 break;
          case 3:cadena =`${e.value} se localiza ${lista.searchRepeat(e.value)} veces`;
                 break;
          case 4:cadena =`El primero de la lista es:${lista.head()}`;
                 break;
          case 5:cadena=`El último de la lista es:${lista.last()}`;
                 break;       
          case 6:cadena =`El resto de la lista es:${lista.tail()}`; 
       }
    else
       cadena="La lista esta vacia";
   }
   document.getElementById("resultados").innerHTML=cadena;
};

let agregar=()=>accion(1);
let buscar=()=>accion(2);
let repetir=()=>accion(3);
let primero=()=>accion(4);
let ultimo=()=>accion(5);
let resto=()=>accion(6);
let longitud=()=>accion(7);

let inicializaEventos=()=>{
    document.getElementById("agregar").addEventListener("click",agregar); 
    document.getElementById("buscar").addEventListener("click",buscar);
    document.getElementById("repetir").addEventListener("click",repetir); 
    document.getElementById("primero").addEventListener("click",primero);
    document.getElementById("ultimo").addEventListener("click",ultimo);
    document.getElementById("resto").addEventListener("click",resto);
    document.getElementById("longitud").addEventListener("click",longitud);
};

inicializaEventos();
