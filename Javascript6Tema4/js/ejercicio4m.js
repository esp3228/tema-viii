/*
    Autor: Rafael Herrera García
    Archivo: ejercicio4m.js
    Descripción: Definición de modulos.
*/

let data=[]

export let addItem= (item)=> data.push(item);

export let searchItem=(item)=> data.filter(x=>x==item).length!=0;

export let count=()=>data.length;

export let searchRepeat=(item)=> data.filter(x=>x==item).length;

export let head=()=>data[0];

export let last=()=>data[data.length-1];

export let tail=()=>data.slice(1);

export let isFree=()=>data.length==0;



