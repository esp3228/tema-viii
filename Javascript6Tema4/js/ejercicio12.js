/*
    Autor: Rafael Herrera García
    Archivo: ejercicio11.js
    Descripción: Definición de clases
*/
import {Taller} from './ejercicio12m.js'
(function(){
     var taller=new Taller('Office 2016','Hilda Garza',1500,'José Pérez',40);
     document.getElementsByClassName("secundario")[0].innerText=taller;
})();
