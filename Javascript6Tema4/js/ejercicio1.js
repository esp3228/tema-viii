/*
    Autor: Rafael Herrera García
    Archivo: ejercicio1.js
    Descripción: Uso de let, var y const.
*/

var miVar=20;
let miLet=30;
const miConst=40;
let d=document.getElementsByTagName("div")[1];

function updateMiVar(){
    d.innerHTML="miVar="+miVar;
    if(true){
        var miVar=10;
    }
    d.innerHTML+="<br>miVar="+miVar;
}

function updateMiLet(){
   d.innerHTML+="<br>miLet="+miLet;
   //let miLet=80;
   if(true){
       let miLet=10;
   }
   d.innerHTML+="<br>miLet="+miLet;
}

function updateMiConst(){
    d.innerHTML+="<br>miConst="+miConst;
    if(true){
        const miConst=10;
    }
    d.innerHTML+="<br>miConst="+miConst;
}

updateMiVar();
updateMiLet();
updateMiConst();