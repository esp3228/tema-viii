/*
    Autor: Rafael Herrera García
    Archivo: ejercicio2.js
    Descripción: Gestionando objetos.
*/

let id=10;
let nombre='Blanca Treviño de la Vega';
let foto='../images/blanca.jpg';
let video="../images/blanca.mp4";
let profesion="Empresaria";

const personaje={
    nombre,
    foto,
    video,
    profesion
};

function muestra(){
    let spn,elemento,contenido;
    d=document.getElementsByTagName("div")[1];
    elemento= document.createElement('h2');
    contenido=document.createTextNode(personaje.nombre);
    elemento.appendChild(contenido);
    contenido=document.createElement("br");
    elemento.appendChild(contenido);
    contenido=document.createTextNode(personaje.profesion);
    elemento.appendChild(contenido);
    d.appendChild(elemento);
    spn=document.createElement("span");
    elemento=document.createElement("img");
    elemento.src=personaje.foto;
    elemento.style.width="300px";elemento.style.height="250px";
    spn.appendChild(elemento);  
    d.appendChild(spn);
    spn=document.createElement('span');
    elemento=document.createElement("video");
    elemento.style.width="355px";elemento.style.height="310px";
    elemento.autoplay=true;
    elemento.controls=true;
    elemento.src=personaje.video;
    contenido=document.createTextNode("El browser no soporta esta etiqueta");
    elemento.appendChild(contenido);
    spn.appendChild(elemento);
    d.appendChild(spn);
}
muestra();