/*
    Autor: Rafael Herrera García
    Archivo: ejercicio8.js
    Descripción: Introducción a las promesas.
*/

(function(){
    function getAlumnos(){
        return new Promise(function(resolve,reject){
             setTimeout(()=>{
                             console.log("Alumnos han sido cargados");
                             resolve();
                            },3000);
        });     
    }

    function getMaterias(){
        return new Promise(function(resolve,reject){
             setTimeout(()=>{
                              console.log("Materias han sido cargadas");
                              resolve();
                            },500);
             });
    }

    function getGrupos(){
        return new Promise(function(resolve,reject){
            setTimeout(()=>{
                            console.log("Grupos han sido cargados");
                            resolve();
                           },300);
        });
    }

    getAlumnos()
             .then(getMaterias)
             .then(getGrupos)
             .catch(function(err){
                 console.log("Error");
             });
    
})();
