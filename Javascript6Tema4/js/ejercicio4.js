/*
    Autor: Rafael Herrera García
    Archivo: ejercicio4.js
    Descripción: Uso de modulo.
*/

import {addItem,
        searchItem,
        searchRepeat,
        head,
        tail,
        last,
        isFree,
        count} from './ejercicio4m.js';




let accion=function (opc){
    let cadena;
    let e=document.getElementById("numero");
    if(opc==1){
        addItem(parseFloat(e.value));
        e.value=1;
        cadena=`Item agregado`;
    }else if(opc==7)
        cadena=`La cantidad de elementos=${count()}`;
    else{
     if(!isFree())   
       switch(opc){
          case 2:cadena=`${e.value} ${searchItem(e.value)?"si":"no"} se localiza`;
                break;
          case 3:cadena =`${e.value} se localiza ${searchRepeat(e.value)} veces`;
                 break;
          case 4:cadena =`El primero de la lista es:${head()}`;
                 break;
          case 5:cadena=`El último de la lista es:${last()}`;
                 break;
          case 6:cadena =`El resto de la lista es:${tail()}`; 
       }
    else
        cadena="La lista esta vacia";
    }
    document.getElementById("resultados").innerHTML=cadena;
};

let agregar=()=>accion(1);
let buscar=()=>accion(2);
let repetir=()=>accion(3);
let primero=()=>accion(4);
let ultimo=()=>accion(5);
let resto=()=>accion(6);
let longitud=()=>accion(7);

let inicializaEventos=()=>{
   document.getElementById("agregar").addEventListener("click",agregar); 
   document.getElementById("buscar").addEventListener("click",buscar);
   document.getElementById("repetir").addEventListener("click",repetir); 
   document.getElementById("primero").addEventListener("click",primero);
   document.getElementById("ultimo").addEventListener("click",ultimo);
   document.getElementById("resto").addEventListener("click",resto);
   document.getElementById("longitud").addEventListener("click",longitud);
};

inicializaEventos();


