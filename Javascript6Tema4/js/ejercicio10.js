/*
    Autor: Rafael Herrera García
    Archivo: ejercicio9.js
    Descripción: Introducción a las promesas con Ajax (fech).
*/

(function(){
    function getDocto(url) {
        return new Promise( (resolve, reject) => {
          fetch(url)
            .then(response => {
              if(response.ok) {
                return response.text();
              }
              reject('No se ha podido acceder a ese recurso. Status: ' + response.status);
            })
            .then( texto => resolve(texto) )
            .catch (err => reject(err) );
        });
      }

      getDocto("../resources/test.txt")
             .then(response=> document.write(response))
             .catch(error=>console.log(error));
    
})();
