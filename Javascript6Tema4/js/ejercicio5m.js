/*
    Autor: Rafael Herrera García
    Archivo: ejercicio5m.js
    Descripción: Definición de modulos.
*/


let data=[]

let addItem= (item)=> data.push(item);

let searchItem=(item)=> data.filter(x=>x==item).length!==0;

let count=()=>data.length;

let searchRepeat=(item)=> data.filter(x=>x==item).length;

let head=()=>data[0];

let last=()=>data[data.length-1];

let tail=()=>data.slice(1);

let isFree=()=>data.length===0;

export const lista={
    addItem,
    searchItem,
    searchRepeat,
    count,
    head,
    last,
    tail,
    isFree
}

