/*
    Autor: Rafael Herrera García
    Archivo: ejercicio3.js
    Descripción: Uso de plantillas de cadenas.
*/

const f=document.getElementsByTagName("div")[1];
let numero,
    cadena;

lecturaNumero=(texto,funcion)=>{
    f.appendChild(document.createTextNode(texto));
    let numero=document.createElement("input");
    numero.setAttribute("type","number");
    numero.setAttribute("id","numero");
    numero.setAttribute("value","1");
    f.appendChild(numero);
    let boton=document.createElement("button");
    boton.appendChild(document.createTextNode("Aceptar"));
    boton.addEventListener("click",funcion);
    f.appendChild(boton);
}


eliminar=(l)=>{ 
      for(i=l.length;i>0;i--)
          l[i-1].remove();
}                

addNumber=()=>{
    numero=parseFloat(document.getElementById("numero").value);
    cadena=`El cubo de ${numero} = ${Math.pow(numero,3)}`;
    eliminar(f.childNodes);
    f.appendChild(document.createTextNode(cadena));
}



lecturaNumero("Proporciona un número:",addNumber);

