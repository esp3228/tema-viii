/*
    Autor: Rafael Herrera García
    Archivo: ejercicio6.js
    Descripción: Uso de canvas de HTML 5.
*/

(function(){
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    function dibuja(){
       ctx.fillStyle="#F00FF0";
       ctx.fillRect(25,25,100,100);
       ctx.clearRect(45,45,60,60);
       ctx.strokeRect(50,50,50,50);
       
       ctx.beginPath();
       ctx.arc(190, 80, 40, 0, 2 * Math.PI);
       ctx.stroke();

       ctx.fillStyle="#000FF0";
       ctx = canvas.getContext("2d");
       ctx.font = "30px Arial";
       ctx.fillText("Hola mundo!!", 250, 75);
       ctx.strokeText("Hola mundo!!", 450, 75);

       var grd = ctx.createLinearGradient(0, 0, 400, 0);
       grd.addColorStop(0, "green");
       grd.addColorStop(1, "yellow");
       ctx.fillStyle = grd;
       ctx.fillRect(25, 250, 390, 150);
    }

    dibuja();
})();
