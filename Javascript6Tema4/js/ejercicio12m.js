/*
    Autor: Rafael Herrera García
    Archivo: ejercicio11.js
    Descripción: Definición de clases
*/

export class Curso{

    constructor(nombre, instructor, costo){
        this.nombre=nombre;
        this.instructor=instructor;
        this.costo=costo;
    }

    obtieneIVA(){
        return this.costo*0.16;
    }


    toString(){
        return `Nombre:${this.nombre}\nInstructor:${this.instructor}\nCosto:${this.costo}`;
    }
}


export class Taller extends Curso{

    constructor (nombre,instructor,costo,auxiliar,tiempo){
        super(nombre,instructor,costo);
        this.auxiliar=auxiliar;
        this.tiempo=tiempo;
    }

    toString(){
        var s=super.toString();
        return `${s}\nAuxiliar:${this.auxiliar}\nTiempo:${this.tiempo}`;
    }

}