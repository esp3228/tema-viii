/*
    Autor: Rafael Herrera García
    Archivo: ejercicio7.js
    Descripción: Ejecución asincrona.
*/

(function(){
    function getAlumnos(){
            setTimeout(()=>console.log("Alumnos han sido cargados"),3000);
    }

    function getMaterias(){
        setTimeout(()=>console.log("Materias han sido cargadas"),500);
    }

    function getGrupos(){
        setTimeout(()=>console.log("Grupos han sido cargados"),300);
    }

    getAlumnos();
    getMaterias();
    getGrupos();
})();
