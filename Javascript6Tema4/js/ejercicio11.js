/*
    Autor: Rafael Herrera García
    Archivo: ejercicio11.js
    Descripción: Definición de clases
*/
import {Curso} from './ejercicio11m.js'
(function(){
     var curso=new Curso('ReactJS','Hernan Sánchez',3500);
     document.getElementsByClassName("secundario")[0].innerText=curso;
})();
