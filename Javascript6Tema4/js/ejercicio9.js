/*
    Autor: Rafael Herrera García
    Archivo: ejercicio9.js
    Descripción: Introducción a las promesas.
*/

(function(){
    function getAlumnos(){
        return new Promise(function(resolve,reject){
             setTimeout(()=>{
                             console.log("Alumnos han sido cargados");
                             resolve(["alondra","juan","pedro","ana"]);
                            },3000);
        });     
    }

    function getMaterias(){
        return new Promise(function(resolve,reject){
             setTimeout(()=>{
                              console.log("Materias han sido cargadas");
                              resolve(["calculo","javascript","css","html5"]);
                            },500);
             });
    }

    function getGrupos(){
        return new Promise(function(resolve,reject){
            setTimeout(()=>{
                            console.log("Grupos han sido cargados");
                            resolve([3,5,7,9,12,34]);
                           },300);
        });
    }

    getAlumnos()
             .then(function(response){
                 console.log(response);
                 return getMaterias();
             })
             .then(function(response){
                 console.log(response);
                 return getGrupos();
             }).then(function(response){
                 console.log(response);
             })
             .catch(function(err){
                 console.log("Error");
             });
    
})();
