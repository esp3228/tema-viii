/*
    Autor: Rafael Herrera García
    Archivo: ejercicio11.js
    Descripción: Definición de clases
*/

export class Curso{

    constructor(nombre, instructor, costo){
        this.nombre=nombre;
        this.instructor=instructor;
        this.costo=costo;
    }

    obtieneIVA(){
        return this.costo*0.16;
    }


    toString(){
        return `Nombre:${this.nombre}\nInstructor:${this.instructor}\nCosto:${this.costo}`;
    }
}


